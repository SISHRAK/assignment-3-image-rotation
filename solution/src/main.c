#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/rotate.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#define bye return 1

void cleanup_all(FILE* input, FILE* output, struct image* img, struct image* rotated_img) {
    if (input) fclose(input);
    if (output) fclose(output);
    if (img && img->data) free(img->data);
    if (img) free(img);
    if (rotated_img && rotated_img->data) free(rotated_img->data);
}
void clean_old(FILE* input, FILE* output, struct image* img){
    if (input) fclose(input);
    if (output) fclose(output);
    if (img && img->data) free(img->data);
    if (img) free(img);
}

int main(int argc, char** argv) {
    if (argc < 4) {
        fprintf(stderr, "Input format: ./image-transformer <source-image> <transformed-image> <angle>\n\n");
        return 1;
    }
    FILE* input = open(argv[1], "rb");
    FILE* output = open(argv[2], "wb");
    int angle = atoi(argv[3]);

    if (!input || !output) {
        fprintf(stderr, "Error");
        fclose(input);
        fclose(output);
        bye;
    }
    struct image* img = malloc(sizeof(struct image));
    if (img == NULL) {
        fprintf(stderr, "Error with image");
        clean_old(input, output, img);
        bye;
    }

    if (from_bmp(input, img) != READ_OK) {
        fprintf(stderr, "Error with reading image");
        clean_old(input, output, img);
        bye;
    }

    struct image rotated_image = rotate(img, angle);

    if (rotated_image.data == NULL) {
        fprintf(stderr, "Error with rotated image");
        cleanup_all(input, output, img, &rotated_image);
        bye;
    }

    if (to_bmp(output, &rotated_image) != WRITE_OK) {
        fprintf(stderr, "Error with writing image");
        cleanup_all(input, output, img, &rotated_image);
        bye;
    }
    cleanup_all(input, output, img, &rotated_image);
    return 0;
}
