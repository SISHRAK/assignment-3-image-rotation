#include "../include/rotate.h"
#include "../include/image.h"
#include <stdlib.h>
struct image rotate(struct image *source, int angle) {
    struct image rotated_image1 = create_image(source->height, source->width);
    struct image rotated_image2 = create_image(source->width, source->height);
    if (angle == 90 || angle == -270) {
        if (rotated_image1.data) {
            struct pixel pixels;
            for (size_t i = 0; i < rotated_image1.height; i++) {
                for (size_t j = 0; j < rotated_image1.width; j++) {
                    pixels = source->data[(j + 1) * source->width - i - 1];
                    rotated_image1.data[i * rotated_image1.width + j] = pixels;
                }
            }
        }
        free(rotated_image2.data);
        return rotated_image1;
    } else if (angle == 180 || angle == -180) {
        if (rotated_image2.data) {
            struct pixel pixels;
            for (size_t i = 0; i < rotated_image2.height; i++) {
                for (size_t j = 0; j < rotated_image2.width; j++) {
                    pixels = source->data[i * source->width + j];
                    rotated_image2.data[source->height * source->width - i * source->width - j - 1] = pixels;
                }
            }
        }
        free(rotated_image1.data);
        return rotated_image2;
    } else if (angle == 270 || angle == -90) {
        if (rotated_image1.data) {
            struct pixel pixels;
            for (size_t i = 0; i < rotated_image1.width; i++) {
                for (size_t j = 0; j < rotated_image1.height; j++) {
                    pixels = source->data[i * source->width + j];
                    rotated_image1.data[(j + 1) * rotated_image1.width - i - 1] = pixels;
                }
            }
        }
        free(rotated_image2.data);
        return rotated_image1;
    } else {
        for (size_t i = 0; i < source->width * source->height; i++) {
            rotated_image2.data[i] = source->data[i];
        }
        free(rotated_image1.data);
        return rotated_image2;
    }
}
