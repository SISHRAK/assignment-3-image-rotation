#include "../include/bmp.h"
#include "../include/defines.h"
#include "../include/file.h"

struct bmp_header create_bmp_header(const struct image *img) {
    struct bmp_header header = {0};
    size_t row_size = img->width * STRUCT_SIZE + get_padding(img->width);
    uint32_t size = img->height * row_size;
    header.bfType = TYPE;
    header.bfileSize = size + BMP_HEADER_SIZE;
    header.bOffBits = BMP_HEADER_SIZE;
    header.biSize = SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = PLANES;
    header.biBitCount = COUNT;
    header.biCompression = 0;
    header.biSizeImage = size;
    header.biXPelsPerMeter = PELS;
    header.biYPelsPerMeter = PELS;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum read_status read_header(FILE *bmp_file, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, bmp_file) == 1) {
        return READ_OK;
    }
    return READ_ERROR;
}

enum read_status read_pixels(FILE *bmp_file, struct image *img) {
    uint32_t padding = get_padding(img->width);

    for (size_t row = 0; row < img->height; ++row) {
        if (fread(img->data + row * img->width, STRUCT_SIZE, img->width, bmp_file) != img->width) {
            return READ_ERROR;
        }
        fseek(bmp_file, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum read_status from_bmp(FILE *bmp_file, struct image *img) {
    struct bmp_header header;
    if (read_header(bmp_file, &header) != READ_OK) {
        return READ_ERROR;
    }

    *img = create_image(header.biWidth, header.biHeight);
    return read_pixels(bmp_file, img);
}

enum write_status write_header(FILE *bmp_file, const struct image *img) {
    struct bmp_header header = create_bmp_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, bmp_file) == 1) {
        return WRITE_OK;
    }
    return WRITE_ERROR;
}

enum write_status write_pixels(FILE *bmp_file, const struct image *img) {
    uint32_t padding = get_padding(img->width);
    unsigned char padding_data[3] = {0};

    for (size_t row = 0; row < img->height; ++row) {
        if (fwrite(img->data + row * img->width, STRUCT_SIZE, img->width, bmp_file) != img->width) {
            return WRITE_ERROR;
        }
        if (padding > 0 && fwrite(padding_data, padding, 1, bmp_file) != 1) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}

enum write_status to_bmp(FILE *bmp_file, struct image *img) {
    if (write_header(bmp_file, img) != WRITE_OK) {
        return WRITE_ERROR;
    }
    return write_pixels(bmp_file, img);
}
