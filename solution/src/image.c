#include "../include/image.h"
#include <stdlib.h>

struct image create_image(const size_t width, const size_t height) {
    struct pixel *pixels = calloc(width * height, sizeof(struct pixel));
    if (pixels == NULL) {
        return (struct image){0, 0, NULL};
    }
    return (struct image){width, height, pixels};
}
