#include "../include/file.h"
#include "../include/image.h"

FILE* open(char* filename, char* mode) {
    return fopen(filename, mode);
}

uint32_t get_padding(uint32_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}
