#ifndef IMAGE_H
#define IMAGE_H

#include <stddef.h>
#include <stdint.h>
//done
struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image create_image(size_t width, size_t height);

#endif
